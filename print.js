const spawn = require('guld-spawn').getSpawn()
const { LEDGERBIN } = require('./util.js')

// print returns the Ledger file 'pretty printed'
function print (file = '-', stdin) {
  return spawn(LEDGERBIN,
    stdin,
    ['-f', file, 'print', '--sort', 'd'])
}

module.exports = print
