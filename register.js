const Papa = require('papaparse')
const { Balance, Account } = require('ledger-types')
const spawn = require('guld-spawn').getSpawn()
const { LEDGERBIN, parseCommodity } = require('./util.js')
const FORMAT = `${[
  '%(quoted(date))',
  '%(effective_date ? quoted(effective_date) : "")',
  '%(code ? quoted(code) : "")',
  '%(cleared ? "true" : "false")',
  '%(pending ? "true" : "false")',
  '%(quoted(payee))',
  '%(quoted(display_account))',
  '%(quoted(amount))'
].join(',')}\n%/,,,,,,${[
  '%(quoted(display_account))',
  '%(quoted(amount))'
].join(',')}\n%/`

function toDate (str) {
  if (str.length === 0) return null
  var date = str.split('/')
  return Date.UTC(date[0], parseInt(date[1], 10) - 1, parseInt(date[2], 10))
}

// register displays all the postings occurring in a single account.
async function register (file = '-', stdin, options) {
  var args = ['-f', file, 'register']
  options = options || {}

  // Allow filtering by a given account name
  if (options.account) {
    args.push(`^${options.account}`)
  }

  args.push('--format')
  args.push(FORMAT)

  var pro = await spawn(LEDGERBIN,
    stdin,
    args)

  var reg = []
  var i = -1
  pro.trim().split('\n').forEach(s => {
    var data = Papa.parse(s, {escapeChar: '\\'}).data[0]
    var amount = parseCommodity(data[7])
    var bal = new Balance(amount)
    var accounts = data[6].split(':')
    var acct = new Account()
    acct._add(bal, accounts)
    if (data[0].length !== 0) {
      i++
      reg[i] = {
        date: toDate(data[0]),
        effectiveDate: toDate(data[1]),
        code: data[2],
        cleared: data[3] === 'true',
        pending: data[4] === 'true',
        payee: data[5],
        postings: [acct]
      }
    } else {
      reg[i].postings.push(acct)
    }
  })
  return reg
}

module.exports = register
