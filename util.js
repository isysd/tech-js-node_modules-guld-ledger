const { Amount } = require('ledger-types')
const LEDGERBIN = '/usr/bin/ledger'
const cre = /^\s+|\s+$/g

// Parse an amount from a given string that looks like one of the following
// cases:
//   £-1,000.00
//   5 STOCKSYMBOL {USD200}
//   -900.00 CAD {USD1.1111111111} [13-Mar-19]
function parseCommodity (data) {
  // Strip out unneeded details.
  data = data.toString()
  data = data.replace(/{.*}/g, '')
  data = data.replace(/\[.*\]/g, '')
  data = data.replace(cre, '')

  // Find the amount first.
  var amountMatch = data.match(/-?[0-9,.]+/)
  if (amountMatch == null) {
    throw new TypeError(`Could not get amount from string: ${data}`)
  }
  var amountString = amountMatch[0]

  // Strip commas and parse amount as an Amount.
  var amount = new Amount(amountString.replace(/,/g, ''), data.replace(amountString, '').replace(cre, ''))
  return amount
}

module.exports = {
  LEDGERBIN: LEDGERBIN,
  parseCommodity: parseCommodity
}
