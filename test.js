/* eslint-env node, mocha */
const assert = require('chai').assert
const version = require('./version.js')
const accounts = require('./accounts.js')
const stats = require('./stats.js')
const print = require('./print.js')
const balance = require('./balance.js')
const register = require('./register.js')
const { includeJournal, LEDGERPATH } = require('./cache.js')
const { Amount } = require('ledger-types')
const { getFS } = require('guld-fs')
var fs

describe('ledger', function () {
  it('version', async function () {
    var ver = await version()
    var mat = ver.substr(0, 5).match(/3\.[0-9]+\.[0-9]+/)
    assert.isTrue(mat !== null)
    assert.isTrue(mat.length === 1)
  })
  it('accounts', async function () {
    var acct = await accounts('fixtures/tx.dat')
    assert.isTrue(Array.isArray(acct))
    assert.isTrue(acct.indexOf('00dani:Assets') >= 0)
    assert.equal(acct.length, 4)
  })
  it('accounts stdin', async function () {
    var acct = await accounts('-', `
2016/06/01 * guld pre-founding contributions
    00dani:Assets                              100 GULD
    00dani:Income:guld                        -100 GULD
    guld:Liabilities                         -100 GULD
    guld:Equity:00dani                          100 GULD
`)
    assert.isTrue(Array.isArray(acct))
    assert.isTrue(acct.indexOf('00dani:Assets') >= 0)
    assert.equal(acct.length, 4)
  })
  it('stats', async function () {
    var stat = await stats('fixtures/tx.dat')
    assert.exists(stat['Unique accounts'])
    assert.exists(stat['Unique payees'])
    assert.equal(stat['Unique payees'], 1)
    assert.equal(stat['Unique accounts'], 4)
  })
  it('print', async function () {
    var l = await print('fixtures/tx.dat')
    assert.exists(l)
    assert.equal(l, `2016/06/01 * guld pre-founding contributions
    00dani:Assets                           100 GULD
    00dani:Income:guld                     -100 GULD
    guld:Liabilities                       -100 GULD
    guld:Equity:00dani                      100 GULD
`)
  })
  it('balance', async function () {
    var bal = await balance('fixtures/tx.dat')
    assert.exists(bal)
    assert.exists(bal.__bal)
    assert.exists(bal['00dani'])
    assert.isTrue(bal['00dani'].Assets.__bal.GULD.equals(new Amount('100', 'GULD')))
  })
  it('register', async function () {
    var reg = await register('fixtures/tx.dat')
    assert.exists(reg)
    assert.isTrue(Array.isArray(reg))
    assert.isTrue(reg.length === 1)
    assert.isTrue(reg[0].postings[0]['00dani'].Assets.__bal.GULD.equals(new Amount('100', 'GULD')))
  })
  it('includeJournal', async function () {
    fs = await getFS()
    try {
      await fs.unlink(LEDGERPATH)
    } catch (e) {
      // ignore, probably didn't exist
    }
    var reg = await includeJournal()
    assert.exists(reg)
    var l = await fs.readFile(LEDGERPATH, 'utf-8')
    assert.isTrue(l.length > 100)
    assert.isTrue(l.indexOf('2016/06/01 * guld pre-founding contributions') >= 0)
  })
})
