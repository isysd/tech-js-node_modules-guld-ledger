# guld-ledger

Guld client for [ledger-cli](http://ledger-cli.org).

> Ledger is a powerful, double-entry accounting system that is accessed from the command line.

## Install

```
npm i guld-ledger
```

### Dependencies

  * [Ledger 3](http://ledger-cli.org/)
  * [Node.js](nodejs.org)
  * [ledger native chrome](https://bitbucket.org/guld/ledger-native) (chrome exension API only)

#### Installing Ledger

##### Ubuntu

Aptitude has an up to date build of ledger, so just install it like normal.

```
apt-get install ledger
```

##### macOS
The simplest way to install Ledger 3 for macOS users is through [Homebrew](http://mxcl.github.com/homebrew/).

```
brew install ledger --HEAD
```

The `--HEAD` option is required to install version 3.x.

## Usage

First import `guld-ledger` or include it in your browser build using your favorite bundler.

```
const { balance, register, cache } = require('guld-ledger')

// cache all ledgers in the given ledger dir
await cache.includeJournal('path/to/ledger/dir')
```

### Available commands

There are six available commands.

* `accounts` - Lists all accounts for postings.
* `balance` - Reports the current balance of all accounts.
* `print` - Prints out the full transactions, sorted by date, using the same format as they would appear in a Ledger data file.
* `register` - Displays all the postings occurring in a single account.
* `stats` - Retrieves statistics, like number of unique accounts.
* `version` - Gets the currently installed Ledger version number.

#### Accounts

Lists all accounts for postings. It returns a readable object `stream`.

```js
ledger.accounts()
  .on('data', function(account) {
    // account is the name of an account (e.g. 'Assets:Current Account')
  })
```

#### Balance

The balance command reports the current balance of all accounts. It returns a [ledger-types](https://bitbucket.org/isysd/ledger-types) Account.

```js
// get the balance for a ledger file
var bal = await balance('path-to-ledger.dat')

// get the balance from journal string
bal = await balance(null, 'raw journal data')
```

#### Print

The print command formats the full list of transactions, ordered by date, using the same format as they would appear in a Ledger data file. It returns a readable stream.

```js
var pretty = await print('path-to-ledger.dat')
```

### Register

The register command displays all the postings occurring in a single account. It returns a readable object `stream`.

```js
var reg = await register()
/*
[
  {
    date: new Date(2014, 1, 1),
    effectiveDate: new Date(2014, 1, 1),
    cleared: true,
    pending: true,
    payee: 'Salary',
    postings: [
      { // a ledger-types.Account object
        __bal: {value: 1,000.00, commodity: '£'} // a ledger-types Balance object
        Assets: {
          __bal: {value: 1,000.00, commodity: '£'}
          Checking: {
            __bal: {value: 1,000.00, commodity: '£'}
          }
        }
      }
    ]
  }
]
*/
```

### Stats

The stats command is used to retrieve statistics about the Ledger data file. It requires a Node style callback function that is called with either an error or the stats object.

```js
var stat = await stats()
```

### Version

The version command is used to get the Ledger binary version. It requires a Node style callback function that is called with either an error or the version number as a string.

```js
// version is a string (e.g. '3.0.0-20130529')
await version()
```
