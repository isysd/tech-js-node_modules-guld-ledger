const spawn = require('guld-spawn').getSpawn()
const { LEDGERBIN } = require('./util.js')

// version reports the current installed Ledger version.
async function version () {
  var v = await spawn(LEDGERBIN, null, ['--version'])
  var matches = v.match(/Ledger (.*),/)
  if (matches) return matches[1]
  else throw new Error('Failed to match Ledger version')
}

module.exports = version
