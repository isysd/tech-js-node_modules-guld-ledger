const Papa = require('papaparse')
const { Balance, Account } = require('ledger-types')
const spawn = require('guld-spawn').getSpawn()
const { LEDGERBIN, parseCommodity } = require('./util.js')
const BALFORMAT = '%(quoted(display_total)),%(quoted(account))\n%/'

// balance reports the current balance of all accounts.
async function balance (file = '-', stdin, options = {}) {
  var args = ['-f', file, 'balance', '--flat', '--format', BALFORMAT]

  if (options.collapse) {
    args.push('--collapse')
  }

  if (options.market) {
    args.push('--market')
  }

  if (options.depth) {
    args.push('--depth')
    args.push(options.depth)
  }

  if (options.query) {
    args.push(options.query)
  }
  var pro = await spawn(LEDGERBIN, stdin, args)
  var account = new Account(null)
  var amtQueue = []
  var result
  pro.split('\n').forEach(s => {
    if (typeof s === 'string' && s.length > 0) {
      if (!s.startsWith('"')) { s = `"${s}` }
      if (!s.endsWith('"')) { s = `${s}"` }
      var data = Papa.parse(s).data
      data.forEach(line => {
        if (line.length === 1) { amtQueue.push(line) }
        if (line.length > 1) {
          var bal = new Balance({})
          bal = bal.add(parseCommodity(line[0]))
          account._add(bal, line[1].split(':'))
          if (amtQueue.length > 0) {
            amtQueue.forEach(amt => {
              bal = new Balance({})
              bal = bal.add(parseCommodity(amt))
              account._add(bal, line[1].split(':'))
            })
            amtQueue = []
          }
        }
      })
    } else {
      result = account
    }
  })
  return result
}

module.exports = balance
