const version = require('./version.js')
const accounts = require('./accounts.js')
const stats = require('./stats.js')
const print = require('./print.js')
const balance = require('./balance.js')
const register = require('./register.js')
const cache = require('./cache.js')

module.exports = {
  version: version,
  accounts: accounts,
  stats: stats,
  print: print,
  balance: balance,
  register: register,
  cache: cache
}
