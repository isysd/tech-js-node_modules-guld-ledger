const { getFS } = require('guld-fs')
const path = require('path')
const home = require('user-home')
const LEDGERDIR = path.join(home, 'ledger')
const LEDGERPATH = path.join(LEDGERDIR, 'GULD', 'ledger.cache')
var fs

// cache and helper functions
async function addToJournal (tx) {
  fs = fs || await getFS()
  await fs.appendFile(LEDGERPATH, `\n${tx}\n`)
}

async function setJournal (j) {
  fs = fs || await getFS()
  return fs.writeFile(LEDGERPATH, j)
}

async function _includeJournal (path, re) {
  fs = fs || await getFS()
  var data = await fs.readFile(path, 'utf8')
  if (!re || re.test(data)) await addToJournal(data)
}

async function includeJournal (p = LEDGERDIR, re) {
  fs = fs || await getFS()
  var list = await fs.readdir(p)
  return Promise.all(list.map(async (l) => {
    var newpath = `${p}/${l}`
    if (l.startsWith('.')) return
    if (l.endsWith('.dat') || l.endsWith('.db')) return _includeJournal(newpath, re)
    var stats = await fs.stat(newpath)
    if (stats.isDirectory()) {
      return includeJournal(newpath, re)
    }
  }))
}

/*
async getAccount (account, cache = true) {
  db = db || defaultDB()
  if (cache) {
    var cacheKey = `ledger_${account}`
    try {
      var acct = await db.getMany([cacheKey])
      if (acct && acct[cacheKey]) {
        this._account[account] = Account.create(acct[cacheKey])
        return this._account[account]
      }
    } catch (e) {
      var result = this.getAccount(account, cache = false)
      return result
    }
  }
  await this.setAccounts((await this.balance()), cache)
  return this._account[account]
}

async setAccounts (accounts, cache = true) {
  db = db || defaultDB()
  if (cache && accounts) {
    accounts = Object.keys(accounts).map(a => {
      var act = {}
      act[`ledger_${a}`] = accounts[a]
      return act
    })
    await db.setMany(accounts, true)
  }
  this._account = accounts
}
*/

module.exports = {
  LEDGERDIR: LEDGERDIR,
  LEDGERPATH: LEDGERPATH,
  addToJournal: addToJournal,
  setJournal: setJournal,
  includeJournal: includeJournal
}
