const spawn = require('guld-spawn').getSpawn()
const { LEDGERBIN } = require('./util.js')

// stats returns statistics, like number of unique accounts
async function stats (file = '-', stdin) {
  var data = await spawn(LEDGERBIN,
    stdin,
    ['-f', file, 'stats'])

  var stats = null
  var split = data.toString().split('\n')
  var files = data.match(/Files these postings came from:([^]*?)(\r?\n){2}/)

  split.forEach(function (el) {
    var prop = el.trim().match(/^(.*):[\s]+(.*)$/)
    if (prop) {
      if (stats === null) {
        stats = {}
      }
      stats[prop[1]] = prop[2]
    }
  })

  if (files) {
    if (stats === null) {
      stats = {}
    }

    // convert files[1] == paths capture to array and remove empty entries
    stats.files = files[1].split('\n').map(function (entry) {
      return entry.trim()
    }).filter(Boolean)
  }

  if (stats !== null) {
    return stats
  } else {
    throw new Error('Failed to parse Ledger stats')
  }
}

module.exports = stats
