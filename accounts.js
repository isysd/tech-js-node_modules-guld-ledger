const spawn = require('guld-spawn').getSpawn()
const { LEDGERBIN } = require('./util.js')

// The accounts command displays the list of accounts used in a Ledger file.
async function accounts (file = '-', stdin) {
  return (await spawn(LEDGERBIN,
    stdin,
    ['-f', file, 'accounts'])
  ).trim().split('\n')
}

module.exports = accounts
